import pymongo
from app.configs.Config import db_config
class BaseRepo:
    def __init__(self):
        self.myclient = pymongo.MongoClient(db_config['url'])
        self.mydb = self.myclient[db_config['name']]
