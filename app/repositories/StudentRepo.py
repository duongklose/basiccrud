import sys
from app.ultis.StudentUtil import StudentUtil
from .__init__ import *


class StudentRepo(BaseRepo):

    def __init__(self, collection: str="students") -> None:
        super().__init__()
        self.collection = self.mydb[collection]

    def get_student(self, student_id):
        student = self.collection.find_one()
        return StudentUtil.format_student(student)

