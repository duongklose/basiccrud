from typing import Optional

from pydantic import BaseModel, EmailStr, Field

print("asdasdasdasdasdasd")
class Student(BaseModel):
    fullname: str = Field(...)
    email: EmailStr = Field(...)
    studentID: str = Field(...)
    gpa: float

class StudentCreate(BaseModel):
    fullname: str
    email: EmailStr
    studentID: str
