from app.repositories.StudentRepo import StudentRepo

class StudentService:
    
    def __init__(self):
        self.__name__= "StudentService"
        self.repo = StudentRepo()
    def get_student(self, student_id):
        return self.repo.get_student(student_id)