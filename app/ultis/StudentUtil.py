import sys
sys.path.append('''E:\Work\TechSoft\BasicCRUD''')
from app.models.Student import Student

class StudentUtil:

    def format_student(student) -> Student:
        return Student(
            fullname=student["fullname"],
            email=student["email"],
            studentID=student["studentID"],
            gpa=student["gpa"]
        )