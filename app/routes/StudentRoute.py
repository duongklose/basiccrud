from fastapi import APIRouter
from app.services.StudentService import StudentService

router = APIRouter()

@router.get("/students")
async def get_students():
    student_service = StudentService()
    return student_service.get_student("123450")