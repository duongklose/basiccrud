from fastapi import FastAPI
import sys
sys.path.append('''E:\Work\TechSoft\BasicCRUD''')

from app.routes import StudentRoute
from app.services.StudentService import StudentService

app = FastAPI()
app.include_router(StudentRoute.router)

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Hello World"}
